
================================

## Build

### Prerequisites

- Node & NPM

### Run locally

- `npm i` - install dependencies
- `npm start` - start development server, visit http://localhost:8088/

*Run production build*

- `npm run preview`

### Production

- `npm run build` to prepare `html`, `css`, `js` files in `dist/` directory

## Credits

