import superagent from 'superagent';

function makeUrl(url) {
    return url;
}

/**
 * @this {function}
 * @param {string} ops.type
 * @param {string} ops.dataType
 * @param {string} ops.contentType
 * @param {string} ops.url
 * @param {json} ops.data
 * @param {function} ops.error
 * @param {function} ops.success
 */
export const ajax = (ops = {}) => {
    const options = {
        url: ops.url,
        type: (ops.type || 'GET').toUpperCase(),
        headers: ops.headers,
        dataType: ops.dataType || 'json',
        data: ops.data || {},
        contentType: ops.contentType || 'application/x-www-form-urlencoded',
        error: ops.error,
        success: ops.success
    };
    let request = superagent(options.type, makeUrl(options.url))
        .set('Content-Type', options.contentType)
        .accept('application/json')
        .on('error', (err) => {
            return options.error ? options.error(err) : null;
        });

    // 请求参数
    if (options.type === 'GET') {
        options.data.t = new Date().getTime();
        request.query(options.data);
    } else {
        request.send(options.data);
    }

    if (options.headers) {
        for (let key in options.headers) {
            request.set(key, options.headers[key]);
        }
    }
    return request.end((err, res) => {
        if (err) {
            return options.error ? options.error(err, res) : null;
        }
        if (!res.body && res.text){
            try {
                res.body = JSON.parse(res.text);
            } catch(ex) {

            }
        }
        if (res.status >= 200 && res.status < 300) {
            try {
                res.body.requestParams = ops.data;
            } catch (e) {
                console.log('res.body null');
            }
            return options.success ? options.success(res.body) : null;
        }
        return options.error ? options.error(err, res) : null;
    });
};



// export const upload = (options) => {
//     let request = superagent.post(options.url);

//     for (let name in options.data) {
//         request = request.field(name, options.data[name]);
//     }

//     return request.attach('file', options.file)
//         .on('progress', e => {
//             return options.progress ? options.progress(e) : null;
//         })
//         .end((err, res) => {
//             if (err) {
//                 return options.error ? options.error(err, res) : null;
//             }

//             if (res.status >= 200 && res.status < 300) {
//                 return options.success ? options.success(res.body) : null;
//             }

//             return options.error ? options.error(err, res) : null;
//         });
// };

export const request = (type, url, params) => {
    return new Promise((resolve, reject) => {
        ajax({
            type: type,
            url: url,
            data: params,
            success: (body) => {
                if (body.code == 0){
                    body.success = true;
                } else {
                    body.success = false;
                }
                resolve(body);
            },
            error: (err, res) => {
                resolve({success: false});
            }
        });
    });
}

export const loadImage = (url) => {
    return new Promise((resolve, reject) => {
        let img = new Image();
        img.src = url;
        // img.id = 'xxx';
        img.onload = (ret) => {
            resolve({success: true, img: img, width: img.width, height: img.height, url: url});
        };
        img.onerror = () => {
            resolve({success: false});
        }
    });
}