import React from 'react';
import { Modal, Input } from 'antd';
const { TextArea } = Input;

class RemarkModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            remark: props.record ? props.record.remark : ''
        }
    }
    onRemarkChange = (e) => {
        this.setState({
            remark: e.target.value
        });
    }
    componentWillReceiveProps(nextProps){
        this.state.remark = nextProps.record ? nextProps.record.remark : '';
    }
    render() {
        return (
            <Modal
              title="添加/修改备注"
              okText="确认"
              cancelText="取消"
              visible={this.props.visible}
              destroyOnClose={true}
              onOk={() => this.props.handleOk(this.state.remark)}
              onCancel={this.props.onCancel}
            >
                <div>
                    <TextArea
                        placeholder="请输入备注"
                        value={this.state.remark}
                        onChange={this.onRemarkChange}
                        autosize={{ minRows: 2}}
                    />
                </div>
            </Modal>
        )
    }
}

export default RemarkModal;