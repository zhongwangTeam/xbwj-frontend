import React from 'react';
import { Select, Checkbox, Table, Spin } from 'antd';
const { Option } = Select;

const cnReg = /[\u4e00-\u9fa5]+/; //中文正则
const tibetanReg = /[\u0f00-\u0fff]+/; // 藏语正则
let splitArticle = (article, len) => {
    if (cnReg.test(article) || tibetanReg.test(article)){
        len = 2*len;
        return article.slice(0, len);
    }
    let tmp = article.split(' ');
    if (tmp.length > len){
        return tmp.slice(0, len).join(' ');
    }
    return article;
}

class DataTable extends React.Component {
    constructor(props) {
        super(props);
    }
    getUrlSummary = (data, field) => {
        let urlHash = {};
        data.map((item, index) => {
            let name = item.alias_name || item[field];
            if (name){
                if (urlHash[name]){
                    urlHash[name] += 1;
                } else {
                    urlHash[name] = 1;
                }
            }
        });
        let sortUrls = [];
        for (let u in urlHash) {
            sortUrls.push([u, urlHash[u]]);
        }
        sortUrls.sort(function(a, b) {
            return b[1] - a[1];
        });
        return sortUrls;
    }
    render() {
        let highlightRegex;
        if (this.props.highlightWords.length > 0){
            highlightRegex = new RegExp(`(${this.props.highlightWords.join('|')})`, 'gi');
        }
        let webColumns = [
            {title: '', dataIndex: 'index', width: '5%', key: '1'},
            {title: () => {
                return (<div className="xxx">网站名称</div>)
            }, dataIndex: 'area', width: '15%', key: '2', render: (text, record) => {
                if (!text){
                    return null;
                }
                if (record.alias_name){
                    return (
                        <div>
                            {record.alias_name}
                            <span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openAliasModal(record); }}> 修改别名</span>
                        </div>
                    )
                } else {
                    return (
                        <div>
                            {text}
                            <span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openAliasModal(record); }}> 使用别名</span>
                        </div>
                    )
                }
            }},
            {title: '作者', dataIndex: 'author', width: '10%', key: '3'},
            {title: '发布时间', dataIndex: 'time', width: '10%', key: '4',sortDirections: ['descend', 'ascend'],
                sorter: (a, b) => (a.time||'').localeCompare(b.time||'')
            },
            {title: '标题', dataIndex: 'title', width: '10%', key: '5', sortDirections: ['descend', 'ascend'],
                render: (text, record) => {
                    let base = splitArticle(text || '', 10);
                    if (!highlightRegex){
                        return (<span className={`${record.tibetan ? 'tibetan' : ''}`}>{base}</span>);
                    }
                    let res = base.replace(highlightRegex, '<span class="highlight">$1</span>');
                    return (<span className={`${record.tibetan ? 'tibetan' : ''}`} dangerouslySetInnerHTML={{__html: res}}></span>);
                },
                sorter: (a, b) => (a.title||'').localeCompare(b.title||'')
            },
            {title: '正文', dataIndex: 'content', key: '6', width: '35%', render: (text, record, index) => {
                let base = splitArticle(text || '[图片或视频]', 40).replace(/\\n|\\r|\\t/g, '');
                if (!highlightRegex){
                    return (<span className={`${record.tibetan ? 'tibetan' : ''}`}>{base}</span>);
                }
                let res = base.replace(highlightRegex, '<span class="highlight">$1</span>');
                return (<span className={`${record.tibetan ? 'tibetan' : ''}`} dangerouslySetInnerHTML={{__html: res}}></span>);
            }},
            {title: '备注', dataIndex: 'remark', width: '10%', key: '8',
                render: (text, record) => {
                    if (!text){
                        return (<span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openRemarkModal(record); }}>添加</span>)
                    } else {
                        return (
                            <div>
                                <span>{text} </span>
                                <span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openRemarkModal(record); }}>修改</span>
                            </div>
                        )
                    }
                }
            },
            {title: '', dataIndex: 'tool', width: '5%', key: '7', render: (text, item, index) => {
                return (<Checkbox onClick={(e) => {e.stopPropagation();}} onChange={(e) => {this.props.onCheckboxChange(e, item, index)}} checked={item.checked}></Checkbox>)
            }}
        ];
        let twColumns = [
            {title: '', dataIndex: 'index', width: '5%', key: '1'},
            {title: '作者', dataIndex: 'tweet_author', width: '15%', key: '2', render: (text, record) => {
                if (!text){
                    return null;
                }
                if (record.alias_name){
                    return (
                        <div>
                            {record.alias_name}
                            <span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openAliasModal(record); }}> 修改别名</span>
                        </div>
                    )
                } else {
                    return (
                        <div>
                            {text}
                            <span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openAliasModal(record); }}> 使用别名</span>
                        </div>
                    )
                }
            }},
            {title: '发布时间', dataIndex: 'tweet_time', width: '10%', key: '3', sortDirections: ['descend', 'ascend'],
                sorter: (a, b) => (a.tweet_time||'').localeCompare(b.tweet_time||'')
            },
            {title: '内容', dataIndex: 'tweet_content', key: '4', width: '35%', render: (text, record, index) => {
                let tmp = '[图片或视频]';
                if (text && text.trim()){
                    tmp = text.trim();
                }
                let base = splitArticle(tmp, 40).replace(/\\n|\\r|\\t/g, '');
                if (!highlightRegex){
                    return (<span>{base}</span>);
                }
                let res = base.replace(highlightRegex, '<span class="highlight">$1</span>');
                return (<span className={`${record.tibetan ? 'tibetan' : ''}`} dangerouslySetInnerHTML={{__html: res}}></span>);
            }},
            {title: '评论次数', dataIndex: 'twitter_reply', width: '10%', key: '5', sortDirections: ['descend', 'ascend'],
                sorter: (a, b) => parseInt(a.twitter_reply||0)-parseInt(b.twitter_reply||0),
                render: (text) => text || 0
            },
            {title: '转发次数', dataIndex: 'twitter_trunsmit', width: '10%', key: '6', sortDirections: ['descend', 'ascend'],
                sorter: (a, b) => parseInt(a.twitter_trunsmit||0)-parseInt(b.twitter_trunsmit||0),
                render: (text) => text || 0
            },
            {title: '备注', dataIndex: 'remark', width: '10%', key: '8',
                render: (text, record) => {
                    if (!text){
                        return (<span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openRemarkModal(record); }}>添加</span>)
                    } else {
                        return (
                            <div>
                                <span>{text} </span>
                                <span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openRemarkModal(record); }}>修改</span>
                            </div>
                        )
                    }
                }
            },
            {title: '', dataIndex: 'tool', width: '5%', key: '7', render: (text, item, index) => {
                return (<Checkbox onClick={(e) => {e.stopPropagation();}} onChange={(e) => {this.props.onCheckboxChange(e, item, index)}} checked={item.checked}></Checkbox>)
            }}
        ];
        let fbColumns = [
            {title: '', dataIndex: 'index', width: '5%', key: '1'},
            {title: '作者', dataIndex: 'author_id', width: '15%', key: '2', render: (text, record) => {
                if (!text){
                    return null;
                }
                if (record.alias_name){
                    return (
                        <div>
                            {record.alias_name}
                            <span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openAliasModal(record); }}> 修改别名</span>
                        </div>
                    )
                } else {
                    return (
                        <div>
                            {text}
                            <span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openAliasModal(record); }}> 使用别名</span>
                        </div>
                    )
                }
            }},
            {title: '发布时间', dataIndex: 'post_date', width: '10%', key: '3', sortDirections: ['descend', 'ascend'],
                sorter: (a, b) => (a.post_date||'').localeCompare(b.post_date||'')
            },
            {title: '内容', dataIndex: 'post_content', width: '35%', key: '4', render: (text, record, index) => {
                let tmp = '[图片或视频]';
                if (text && text.trim()){
                    tmp = text.trim();
                }
                let base = splitArticle(tmp, 40).replace(/\\n|\\r|\\t/g, '');
                if (!highlightRegex){
                    return (<span className={`${record.tibetan ? 'tibetan' : ''}`}>{base}</span>);
                }
                let res = base.replace(highlightRegex, '<span class="highlight">$1</span>');
                return (<span className={`${record.tibetan ? 'tibetan' : ''}`} dangerouslySetInnerHTML={{__html: res}}></span>);
            }},
            {title: '评论次数', dataIndex: 'post_comment_sum', width: '10%', key: '5', sortDirections: ['descend', 'ascend'],
                sorter: (a, b) => parseInt(a.post_comment_sum||0)-parseInt(b.post_comment_sum||0)
            },
            {title: '转发次数', dataIndex: 'post_shared', width: '10%', key: '6', sortDirections: ['descend', 'ascend'],
                sorter: (a, b) => parseInt(a.post_shared||0)-parseInt(b.post_shared||0),
                render: (text) => text || 0
            },
            {title: '备注', dataIndex: 'remark', width: '10%', key: '8',
                render: (text, record) => {
                    if (!text){
                        return (<span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openRemarkModal(record); }}>添加</span>)
                    } else {
                        return (
                            <div>
                                <span>{text} </span>
                                <span className="clickable" onClick={(e) => { e.stopPropagation(); this.props.openRemarkModal(record); }}>修改</span>
                            </div>
                        )
                    }
                }
            },
            {title: '', dataIndex: 'tool', width: '5%', key: '7', render: (text, item, index) => {
                return (<Checkbox onClick={(e) => {e.stopPropagation();}} onChange={(e) => {this.props.onCheckboxChange(e, item, index)}} checked={item.checked}></Checkbox>)
            }}
        ];
        let columns = webColumns;
        if (this.props.type == 'facebook'){
            columns = fbColumns;
            let sortUrls = this.getUrlSummary(this.props.sourceData, 'author_id');
            let options = sortUrls.map(function(item, index){
                return (<Option key={index} value={item[0]}>{index+1}: {item[0]} {item[1]}</Option>);
            });
            columns[1].title = () => {
                return (
                    <div>
                        <span>作者</span>
                        <Select onChange={this.props.onFacebookUrlChange} dropdownMatchSelectWidth={false} style={{width: 72}} defaultValue="all"><Option value="all">全部</Option>{options}</Select>
                    </div>
                )
            }
        } else if (this.props.type == 'twitter'){
            columns = twColumns;
            let sortUrls = this.getUrlSummary(this.props.sourceData, 'tweet_author');
            let options = sortUrls.map(function(item, index){
                return (<Option key={index} value={item[0]}>{index+1}: {item[0]} {item[1]}</Option>);
            });
            columns[1].title = () => {
                return (
                    <div>
                        <span>作者</span>
                        <Select onChange={this.props.onTwitterUrlChange} dropdownMatchSelectWidth={false} style={{width: 72}} defaultValue="all"><Option value="all">全部</Option>{options}</Select>
                    </div>
                )
            }
        } else if (this.props.type == 'web'){
            let sortUrls = this.getUrlSummary(this.props.sourceData, 'area');
            let options = sortUrls.map(function(item, index){
                return (<Option key={index} value={item[0]}>{index+1}: {item[0]} {item[1]}</Option>);
            });
            columns[1].title = () => {
                return (
                    <div>
                        <span>网站名称 </span>
                        <Select onChange={this.props.onWebUrlChange} dropdownMatchSelectWidth={false} style={{width: 72}} defaultValue="all"><Option value="all">全部</Option>{options}</Select>
                    </div>
                )
            }
        }
        let dataSource = this.props.data.map((item, index) => {
            item.index = index + 1;
            item.key = index;
            // if (this.props.type == 'web'){
            //     item.content = splitArticle(item.content || '[图片或视频]', 40);
            //     item.title = splitArticle(item.title || '', 10);
            // } else if (this.props.type == 'facebook'){
            //     item.post_content = splitArticle(item.post_content || '[图片或视频]', 40);
            //     item.post_title = splitArticle(item.post_title || '', 10);
            // }
            return item;
        });
        let tableScrollHeight = this.props.innerHeight - 88 - 65;
        if (this.props.showDetail){
            //屏幕高度 - 顶部 - 表头高度 - 详情高度
            tableScrollHeight = this.props.innerHeight - 88 - 65 - this.props.detailWinHeight;
        }
        return (
            <div>
                <Spin spinning={this.props.loading}>
                    <Table
                        pagination={false}
                        scroll={{ y: tableScrollHeight }}
                        rowClassName={
                            (item, index) => {
                                let clazz = [];
                                if (index === this.props.currentDetailRow){
                                    clazz.push('current');
                                }
                                if (item.isRead){
                                    clazz.push('readed');
                                }
                                return clazz.join(' ');
                            }
                        }
                        dataSource={dataSource}
                        columns={columns}
                        onChange={this.props.onTableChange}
                        onRow={(record, index) => {
                            return {
                                onClick: event => {
                                    this.props.onClick(record, index);
                                }
                            };
                        }}
                    />
                </Spin>
            </div>
        );
    }
}

export default DataTable;