import React from 'react';
import { Modal, Input } from 'antd';
const { TextArea } = Input;

class AliasModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            alias: props.record ? props.record.alias_name : ''
        }
    }
    onAliasChange = (e) => {
        this.setState({
            alias: e.target.value
        });
    }
    componentWillReceiveProps(nextProps){
        this.state.alias = nextProps.record ? nextProps.record.alias_name : '';
    }
    _renderTitle() {
        let record = this.props.record;
        return (<div style={{marginBottom: 20}}>为「{record.area || record.author_id || record.tweet_author}」使用别名</div>)
    }
    render() {
        if (!this.props.visible){
            return null;
        }
        return (
            <Modal
              title="添加/修改别名"
              okText="确认"
              cancelText="取消"
              visible={this.props.visible}
              destroyOnClose={true}
              onOk={() => this.props.handleOk(this.state.alias)}
              onCancel={this.props.onCancel}
            >
                <div>
                    {this._renderTitle()}
                    <TextArea
                        placeholder="请输入别名"
                        value={this.state.alias}
                        onChange={this.onAliasChange}
                        autosize={{ minRows: 1}}
                    />
                </div>
            </Modal>
        )
    }
}

export default AliasModal;