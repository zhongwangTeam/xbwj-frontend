import React from 'react';
import { Modal, Input, Button, message } from 'antd';

class KeywordModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
    }
    onInputChange = (e) => {
        this.setState({value: e.target.value})
    }
    onAdd = () => {
        let kw = this.state.value;
        let reg = new RegExp(kw, 'i');
        let keywordsStr = (this.props.keywords || []).join('');
        if (reg.test(keywordsStr)){
            message.warn('标签已存在');
            return false;
        }
        this.setState({value: ''});
        this.props.onAdd(this.state.value);
    }
    render() {
        if (!this.props.visible){ return null; }
        let tmp = this.props.keywords || [];
        let el;
        if (tmp.length == 0){
            el = <div style={{textAlign: 'center'}}>暂无关键字</div>;
        } else {
            el = tmp.map((item, index) => {
                return (
                    <div key={index} className="keyword">{item} <span onClick={()=>{this.props.onRemoveKeyword(item, index)}}> 删除</span></div>
                )
            });
        }
        return (
            <Modal
              title="关键字管理"
              okText="确认"
              cancelText="取消"
              visible={this.props.visible}
              onOk={this.props.handleOk}
              onCancel={this.props.onCancel}
            >
                <div className="kw-add-box">
                    <Input value={this.state.value} onChange={this.onInputChange} style={{width: 120}} placeholder="关键字名称" />
                    <Button onClick={this.onAdd} className="kw-add-btn">添加</Button>
                </div>
                <div className="km-keywords clearfix">
                    {el}
                </div>
            </Modal>
        )
    }
}

export default KeywordModal;