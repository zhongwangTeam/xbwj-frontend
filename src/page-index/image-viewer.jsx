import React from 'react';

class ImageViewer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    
    render() {
        if (!this.props.visible){ return null; }
        let imgWidth = this.props.image.width;
        let imgHeight = this.props.image.height;
        let rate = imgWidth/imgHeight;
        let { innerWidth, innerHeight } = window;
        if (imgWidth > innerWidth*0.8){
            imgWidth = innerWidth*0.8;
            imgHeight = imgWidth/rate;
        }
        let style = {
            position: 'absolute',
            left: (innerWidth-imgWidth)/2,
            top: (innerHeight-imgHeight)/2,
            width: imgWidth,
            height: imgHeight,
        };
        return (
            <div className="image-viewer">
                <div className="image-viewer-inner">
                    <div onClick={this.props.onClose} className="image-mask"></div>
                    <div style={style}>
                        <img style={{width: imgWidth}} src={this.props.image.url} />
                    </div>
                </div>
            </div>
        )
    }
}

export default ImageViewer;