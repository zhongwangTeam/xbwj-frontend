// require('normalize.css/normalize.css');
require('../../node_modules/antd/dist/antd.min.css');
require('../css/main.css');
import moment from 'moment';
import 'moment/locale/zh-cn';
moment.locale('zh-cn');
let avatarSrc = require('../images/account.png');
let dragSrc = require('../images/drag.png');
import { request, loadImage } from '../utils/request';

import { hot } from 'react-hot-loader';
import React from 'react';
import ReactDOM from 'react-dom';
import { Button, Select, DatePicker, Input, Checkbox, Table, Modal, message, Spin } from 'antd';
const { Option } = Select;
import locale from 'antd/lib/date-picker/locale/zh_CN';
import DetailBox from './detail.jsx';
import DataTable from './data-table.jsx';
import KeywordModal from './kw-modal.jsx';
import DataDeleteModal from './data-delete-modal.jsx';
import ImageViewer from './image-viewer.jsx';
import RemarkModal from './remark-modal.jsx';
import AliasModal from './alias-modal.jsx';

let urls = {
    web: {count: '/web/dataCount', list: '/web/getDatas'},
    twitter: {count: '/twitter/dataCount', list: '/twitter/getDatas'},
    facebook: {count: '/facebook/dataCount', list: '/facebook/getDatas'}
};
const tibetanReg = /[\u0f00-\u0fff]+/; // 藏语正则

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'web',
            loading: false,
            startDateMode: 'date',
            endDateMode: 'date',
            startTime: moment().startOf('day'),
            endTime: moment().endOf('day'),
            sourceData: [],
            tableData: [],
            kmVisible: false,
            ddVisible: false,
            keywords: [],
            checkedKeywords: [],
            filterString: '',
            selectAll: false,
            showDetail: false,
            detail: null,
            innerHeight: window.innerHeight,
            detailWinHeight: 300,
            detailAuthor: {},
            detailComments: [],
            detailFollowers: [],
            detailFollowing: [],
            detailFriends: [],
            detailLoading: false,
            ddConfirmLoading: false,
            currentDetailRow: '',
            imageVisible: false,
            showRemarkModal: false
        };
    }
    async componentDidMount() {
        this.setState({loading: true});
        let list = await request('GET', urls[this.state.type]['list'], {
            startTime: this.state.startTime.format('YYYY-MM-DD HH:mm'),
            endTime: this.state.endTime.format('YYYY-MM-DD HH:mm'),
        });
        let keywords = await request('GET', '/config/getKeyWords', {});
        this.setState({
            loading: false,
            sourceData: list.data,
            tableData: list.data,
            keywords: keywords.data
        });
        window.addEventListener('resize',  this.handleResize.bind(this));
    }
    handleResize() {
        this.setState({
            innerHeight: window.innerHeight
        })
    }
    onChange() {

    }
    handleStartPanelChange = (value, mode) => {
        this.setState({startDateMode: mode});
    }
    handleEndPanelChange = (value, mode) => {
        this.setState({endDateMode: mode});
    }
    onTypeChange = async (value) => {
        this.setState({type: value, loading: true, showDetail: false, currentDetailRow: '', checkedKeywords: []});
        let list = await request('GET', urls[value]['list'], {
            startTime: this.state.startTime.format('YYYY-MM-DD HH:mm'),
            endTime: this.state.endTime.format('YYYY-MM-DD HH:mm'),
        });
        this.setState({
            loading: false,
            filterString: '',
            sourceData: list.data,
            tableData: list.data
        });
    }
    onOpenKeywordModal = () => {
        this.setState({kmVisible: true, showDetail: false, currentDetailRow: ''});
    }
    onOpenDataDeleteModal = () => {
        this.setState({ddVisible: true, showDetail: false, currentDetailRow: ''});
    }
    onCloseKeywordModal = () => {
        this.setState({kmVisible: false});
    }
    onDataDeleteModal = () => {
        this.setState({ddVisible: false});
    }
    onRemoveKeyword = async (item, index) => {
        let res = await request('GET', '/config/deleteKeyWord', {keyWord: item});
        message.info('已删除');
        this.state.keywords.splice(index, 1);
        let cindex = this.state.checkedKeywords.indexOf(item);
        if (cindex >= 0){
            this.state.checkedKeywords.splice(cindex, 1);
        }
        this.setState({
            keywords: this.state.keywords,
            checkedKeywords: this.state.checkedKeywords
        });
    }
    onTableRowClick = async (item, index) => {
        this.setState({
            showDetail: true,
            detailLoading: true,
            detail: this.state.tableData[index],
            currentDetailRow: index
        });
        if (this.state.type == 'facebook'){
            let res1 = await request('GET', `/facebook/getIntroduction`, {userId: item.author_id});
            let res2 = await request('GET', `/facebook/getComments`, {postId: item.post_id});
            let res3 = await request('GET', `/facebook/getFriends`, {userId: item.author_id});
            this.setState({
                // detailLoading: false,
                detailAuthor: res1.data || {},
                detailComments: res2.data || [],
                detailFriends: res3.data || []
            });
        } else if (this.state.type == 'twitter'){
            let res1 = await request('GET', `/twitter/getAboutData`, {userId: item.account_id});
            let res2 = await request('GET', `/twitter/getCommentData`, {tweetId: item.tweet_id});
            let res3 = await request('GET', `/twitter/getFollowerData`, {userId: item.account_id});
            let res4 = await request('GET', `/twitter/getFollowingData`, {userId: item.account_id});
            this.setState({
                // detailLoading: false,
                detail: this.state.tableData[index],
                detailAuthor: res1.data || {},
                detailComments: res2.data || [],
                detailFollowers: res3.data || [],
                detailFollowing: res4.data || []
            });
        }
        this.setState({
            detailLoading: false
        });
        
        if (this.state.tableData[index].isRead){
            //do nothing
        } else {
            await request('GET', `/${this.state.type}/read`, {id: item.id});
            this.state.tableData[index].isRead = 1;
            this.setState({
                tableData: this.state.tableData
            });
        }
    }
    onWebUrlChange = (value) => {
        if (value == 'all'){
            this.setState({
                showDetail: false,
                currentDetailRow: '',
                tableData: this.state.sourceData
            });
            return;
        }
        let tmp = this.state.sourceData.filter((item) => {
            return item.area == value || item.alias_name == value;
        });
        this.setState({
            showDetail: false,
            currentDetailRow: '',
            tableData: tmp
        });
    }
    onFacebookUrlChange = (value) => {
        if (value == 'all'){
            this.setState({
                showDetail: false,
                currentDetailRow: '',
                tableData: this.state.sourceData
            });
            return;
        }
        let tmp = this.state.sourceData.filter((item) => {
            return item.author_id == value || item.alias_name == value;
        });
        this.setState({
            showDetail: false,
            currentDetailRow: '',
            tableData: tmp
        });
    }
    onTwitterUrlChange = (value) => {
        if (value == 'all'){
            this.setState({
                showDetail: false,
                currentDetailRow: '',
                tableData: this.state.sourceData
            });
            return;
        }
        let tmp = this.state.sourceData.filter((item) => {
            return item.tweet_author == value || item.alias_name == value;
        });
        this.setState({
            showDetail: false,
            currentDetailRow: '',
            tableData: tmp
        });
    }
    onStartDateChange = (value) => {
        this.state.startTime = value;
    }
    onEndDateChange = (value) => {
        this.state.endTime = value;
    }
    onDateChange = async () => {
        if (this.state.startTime.valueOf() > this.state.endTime.valueOf()){
            return message.warn('起始时间不能大于截止日期');
        }
        if (this.state.endTime.diff(this.state.startTime, 'days') > 30){
            return message.warn('时间间隔不能大于 30 天');
        }
        this.setState({loading: true, showDetail: false, currentDetailRow: '', checkedKeywords: []});
        let list = await request('GET', urls[this.state.type]['list'], {
            startTime: this.state.startTime.format('YYYY-MM-DD HH:mm'),
            endTime: this.state.endTime.format('YYYY-MM-DD HH:mm'),
        });
        this.setState({
            loading: false,
            filterString: '',
            sourceData: list.data,
            tableData: list.data,
        });
    }
    onFilterChange = (e) => {
        let value = e.target.value;
        if (value == ''){
            this.setState({
                filterString: value,
                tableData: this.state.sourceData
            });
            return;
        }
        let reg = new RegExp(value, 'i');
        let tmp = this.state.sourceData.filter((item) => {
            let source = item.content || '';
            if (this.state.type == 'facebook'){
                source = item.post_content || '';
            } else if (this.state.type == 'twitter'){
                source = item.tweet_content || '';
            }
            return reg.test(source);
        });
        this.setState({
            filterString: value,
            tableData: tmp
        });
    }
    onExport = () => {
        let checked = [];
        this.state.tableData.map((item) => {
            if (item.checked){
                checked.push(item.id);
            }
        });
        if (checked.length == 0){
            return message.warn('请选择数据再导出');
        }
        let redirectWindow = window.open(`/${this.state.type}/exports?ids=${checked.join(',')}`, '_blank');
        redirectWindow.location;
    }
    onCheckboxChange = (e, item, index) => {
        this.state.tableData[index]['checked'] = e.target.checked;
        this.setState({
            tableData: this.state.tableData
        });
    }
    onSelectAll = () => {
        let checked = !this.state.selectAll;
        this.state.tableData.map((item) => {
            item.checked = checked;
        });
        this.state.selectAll = checked;
        this.setState({
            showDetail: false,
            currentDetailRow: '',
            tableData: this.state.tableData
        });
    }
    onCloseDetail = () => {
        this.setState({
            currentDetailRow: '',
            showDetail: false
        });
    }
    onAddKeyword = async (value) => {
        let keyword = await request('GET', '/config/addKeyWord', {keyWord: value});
        message.info('已添加');
        this.state.keywords.push(value);
        this.setState({keywords: this.state.keywords});
    }
    onKeywordFilterChange = async (e) => {
        let kw = e.target.value;
        let checked = e.target.checked;
        let index = this.state.checkedKeywords.indexOf(kw);
        if (checked && index < 0){
            this.state.checkedKeywords.push(kw);
        } else if (!checked && index >= 0){
            this.state.checkedKeywords.splice(index, 1);
        }
        let params = {
            startTime: this.state.startTime.format('YYYY-MM-DD HH:mm'),
            endTime: this.state.endTime.format('YYYY-MM-DD HH:mm'),
        };
        if (this.state.checkedKeywords.length > 0){
            params.keyWords = this.state.checkedKeywords.join(',');
        }
        this.setState({
            loading: true,
            filterString: '',
            checkedKeywords: this.state.checkedKeywords,
            showDetail: false,
            currentDetailRow: '',
        });
        let list = await request('GET', urls[this.state.type]['list'], params);
        this.setState({
            loading: false,
            sourceData: list.data,
            tableData: list.data
        });
    }
    onDeleteData = async (data) => {
        let params = {
            endTime: data.endTime
        };
        if (data.startTime){
            params.startTime = data.startTime
        }
        this.setState({ddConfirmLoading: true});
        let res = await request('GET', `/${data.type}/deleteDatas`, params);
        this.setState({
            ddConfirmLoading: false,
            ddVisible: false
        });
        message.info('删除成功');
    }
    onImageClick = async (src) => {
        let res = await loadImage(src);
        if (!res.success){return false;}
        this.setState({
            imageVisible: true,
            showImage: res
        });
    }
    onImageViewerClose = () => {
        this.setState({
            imageVisible: false,
            showImage: null
        });
    }
    onDetailWinChange = (data) => {
        this.setState({
            detailWinHeight: data.height
        });
    }
    onTranslate = async (type, data) => {
        // let mayTrans = true;
        // if (type == 'web'){
        //     if (tibetanReg.test(data.content)){
        //         mayTrans = false;
        //     }
        // } else if (type == 'facebook'){
        //     if (tibetanReg.test(data.post_content)){
        //         mayTrans = false;
        //     }
        // } else if (type == 'twitter'){
        //     if (tibetanReg.test(data.tweet_content)){
        //         mayTrans = false;
        //     }
        // }
        // if (!mayTrans){
        //     return message.warn('暂不支持藏文翻译');
        // }
        this.setState({detailLoading: true});
        let res = await request('GET', `/translate/${type}`, {id: data.id});
        if (!res.success){
            this.setState({
                detailLoading: false,
            });
            return message.warn('翻译失败');
        }
        let index = this.state.tableData.findIndex((item) => item.id == data.id);
        if (type == 'web'){
            this.state.tableData[index]['content_cn'] = res.data.content_cn;
            this.state.tableData[index]['title_cn'] = res.data.title_cn;
        } else if (type == 'facebook'){
            this.state.tableData[index]['post_content_cn'] = res.data.post_content_cn;
            this.state.tableData[index]['post_title_cn'] = res.data.post_title_cn;
        } else if (type == 'twitter'){
            this.state.tableData[index]['tweet_content_cn'] = res.data.tweet_content_cn;
        }
        this.setState({
            detailLoading: false,
            tableData: this.state.tableData
        })
    }
    openRemarkModal = (record) => {
        this.state.remarkRecord = record;
        this.setState({showRemarkModal: true});
    }
    closeRemarkModal = () => {
        this.setState({showRemarkModal: false});
    }
    addRemark = async (remark) => {
        let type = this.state.type;
        let record = this.state.remarkRecord;
        let res = await request('GET', `/${type}/setRemark`, {id: record.id, remark});
        if (res.success){
            this.setState({showRemarkModal: false});
            let index = this.state.tableData.findIndex((item) => item.id == record.id);
            this.state.tableData[index]['remark'] = remark;
            this.setState({
                showRemarkModal: false,
                tableData: this.state.tableData
            });
            message.info('备注已修改');
        } else {
            message.warn('备注修改失败');
        }
    }
    openAliasModal = (record) => {
        this.state.aliasRecord = record;
        this.setState({showAliasModal: true});
    }
    closeAliasModal = () => {
        this.setState({showAliasModal: false});
    }
    setAliasName = async (alias) => {
        let type = this.state.type;
        let record = this.state.aliasRecord;
        let params = {aliasName: alias};
        if (type == 'web'){
            params.area = record.area;
        } else if (type == 'facebook'){
            params.authorId = record.author_id;
        } else if (type == 'twitter'){
            params.accountId = record.account_id;
        }
        let res = await request('GET', `/${type}/setAliasName`, params);
        if (res.success){
            this.setState({showAliasModal: false});
            this.state.tableData.map((item) => {
                if (type == 'web' && item.area == record.area){
                    item.alias_name = alias;
                } else if (type == 'facebook' && item.author_id == record.author_id){
                    item.alias_name = alias;
                } else if (type == 'twitter' && item.account_id == record.account_id){
                    item.alias_name = alias;
                }
            });
            this.setState({
                showAliasModal: false,
                tableData: this.state.tableData
            });
            message.info('别名已修改');
        } else {
            message.warn('别名修改失败');
        }
    }
    onTableChange = (pagination, filters, sorter, extra) => {
        this.setState({
            currentDetailRow: '',
            showDetail: false
        });
        this.state.tableData = extra.currentDataSource;
    }
    onCloseTranslate = (data) => {
        this.state.detail.closeTranslate = true
        this.setState({
            detail: this.state.detail
        })
    }
    
    render() {
        let cbs = this.state.keywords.map((item, index) => {
            return (
                <Checkbox key={index} checked={this.state.checkedKeywords.indexOf(item) >= 0} onChange={this.onKeywordFilterChange} value={item}>{item}</Checkbox>
            );
        });
        let highlightWords = [...this.state.checkedKeywords];
        if (this.state.filterString){
            highlightWords.push(this.state.filterString);
        }
        // let style = {};
        // if (this.state.showDetail){
        //     style.marginBottom = this.state.detailWinHeight;
        // }
        this.state.tableData.map((item) => {
            if (this.state.type == 'web'){
                if (tibetanReg.test(item.content)){
                    item.tibetan = true;
                }
            } else if (this.state.type == 'facebook'){
                if (tibetanReg.test(item.post_content)){
                    item.tibetan = true;
                }
            } else if (this.state.type == 'twitter'){
                if (tibetanReg.test(item.tweet_content)){
                    item.tibetan = true;
                }
            }
        });
        return (
            <div className="container">
                <div className="top-area">
                    <div style={{padding: 10}}>
                        <div className="headbox clearfix">
                            <div className="typebox fl">
                                <Select value={this.state.type} style={{ width: 120 }} onChange={this.onTypeChange}>
                                    <Option value="web">Web</Option>
                                    <Option value="twitter">Twitter</Option>
                                    <Option value="facebook">Facebook</Option>
                                </Select>
                            </div>
                            <div className="datebox fl">
                                发布日期: &nbsp;
                                <DatePicker
                                    allowClear={false}
                                    mode={this.state.startDateMode}
                                    defaultValue={this.state.startTime}
                                    locale={locale}
                                    showTime
                                    onChange={this.onStartDateChange}
                                    onPanelChange={this.handleStartPanelChange}
                                />
                                &nbsp;~&nbsp;
                                <DatePicker
                                    allowClear={false}
                                    mode={this.state.endDateMode}
                                    defaultValue={this.state.endTime}
                                    locale={locale}
                                    showTime
                                    onChange={this.onEndDateChange}
                                    onPanelChange={this.handleEndPanelChange}
                                />
                                &nbsp;
                                <Button onClick={this.onDateChange}>确定</Button>
                            </div>
                            
                            <div className="tools fr">
                                <Button onClick={this.onOpenKeywordModal} className="keywords">关键字管理</Button>
                                <Button style={{marginRight: 10}} onClick={this.onOpenDataDeleteModal} className="export">数据删除</Button>
                                <Button onClick={this.onExport} className="export">导出</Button>
                            </div>
                            <div className="fr">
                                <Input placeholder="内容筛选" allowClear value={this.state.filterString} onChange={this.onFilterChange} />
                            </div>
                        </div>
                        <div className="neckbox clearfix">
                            <div className="keywords fl">
                                <span>关键字: </span>
                                {cbs}
                            </div>
                            <div className="fr">
                                <Button onClick={this.onSelectAll}>全选</Button> 
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <DataTable
                        type={this.state.type}
                        loading={this.state.loading}
                        data={this.state.tableData}
                        sourceData={this.state.sourceData}
                        onClick={this.onTableRowClick}
                        onWebUrlChange={this.onWebUrlChange}
                        onFacebookUrlChange={this.onFacebookUrlChange}
                        onTwitterUrlChange={this.onTwitterUrlChange}
                        onCheckboxChange={this.onCheckboxChange}
                        currentDetailRow={this.state.currentDetailRow}
                        highlightWords={highlightWords}
                        openRemarkModal={this.openRemarkModal}
                        openAliasModal={this.openAliasModal}
                        innerHeight={this.state.innerHeight}
                        showDetail={this.state.showDetail}
                        detailWinHeight={this.state.detailWinHeight}
                        onTableChange={this.onTableChange}
                    />
                </div>
                <KeywordModal
                    visible={this.state.kmVisible}
                    onCancel={this.onCloseKeywordModal}
                    keywords={this.state.keywords}
                    onRemoveKeyword={this.onRemoveKeyword}
                    onAdd={this.onAddKeyword}
                    handleOk={this.onCloseKeywordModal}
                />
                <DetailBox
                    type={this.state.type}
                    visible={this.state.showDetail}
                    data={this.state.detail}
                    onClose={this.onCloseDetail}
                    author={this.state.detailAuthor}
                    comments={this.state.detailComments}
                    following={this.state.detailFollowing}
                    followers={this.state.detailFollowers}
                    friends={this.state.detailFriends}
                    detailLoading={this.state.detailLoading}
                    onImageClick={this.onImageClick}
                    highlightWords={highlightWords}
                    onDetailWinChange={this.onDetailWinChange}
                    onTranslate={this.onTranslate}
                    onCloseTranslate={this.onCloseTranslate}
                />
                <DataDeleteModal
                    visible={this.state.ddVisible}
                    onCancel={this.onDataDeleteModal}
                    handleOk={this.onDeleteData}
                    confirmLoading={this.state.ddConfirmLoading}
                />
                <RemarkModal
                    visible={this.state.showRemarkModal}
                    onCancel={this.closeRemarkModal}
                    handleOk={this.addRemark}
                    record={this.state.remarkRecord}
                />
                <AliasModal
                    visible={this.state.showAliasModal}
                    onCancel={this.closeAliasModal}
                    handleOk={this.setAliasName}
                    record={this.state.aliasRecord}
                />
                <ImageViewer
                    visible={this.state.imageVisible}
                    onClose={this.onImageViewerClose}
                    image={this.state.showImage}
                />
            </div>
        );
    }
}

export default hot(module)(App);