import React from 'react';
import { Modal, Radio, DatePicker, message } from 'antd';
import moment from 'moment';
import locale from 'antd/lib/date-picker/locale/zh_CN';

class DataDeleteModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'web',
            startDateMode: 'date',
            endDateMode: 'date',
            endTime: moment().endOf('day'),
        }
    }

    onChange = (e) => {
        this.setState({
            type: e.target.value,
        });
    }
    handleStartPanelChange = (value, mode) => {
        this.setState({startDateMode: mode});
    }
    handleEndPanelChange = (value, mode) => {
        this.setState({endDateMode: mode});
    }
    onStartDateChange = (value) => {
        this.state.startTime = value;
    }
    onEndDateChange = (value) => {
        this.state.endTime = value;
    }
    handleOk = () => {
        if (this.state.startTime && this.state.endTime){
            if (this.state.endTime.valueOf() < this.state.startTime.valueOf()){
                return message.warn('截止时间不能大于开始时间');
            }
        }
        this.props.handleOk({
            startTime: this.state.startTime ? this.state.startTime.format('YYYY-MM-DD HH:mm') : undefined,
            endTime: this.state.endTime.format('YYYY-MM-DD HH:mm'),
            type: this.state.type
        });
    }
    render() {
        
        return (
            <Modal
              title="数据删除"
              okText="确认"
              cancelText="取消"
              visible={this.props.visible}
              onOk={this.handleOk}
              onCancel={this.props.onCancel}
              confirmLoading={this.props.confirmLoading}
            >
                <div className="">
                    <div className="dd-row"><span>开始日期: </span>
                        <DatePicker
                            allowClear={true}
                            mode={this.state.startDateMode}
                            locale={locale}
                            showTime
                            onChange={this.onStartDateChange}
                            onPanelChange={this.handleStartPanelChange}
                        />
                    </div>
                    <div className="dd-row">
                        <span>截止日期: </span>
                        <DatePicker
                            allowClear={false}
                            mode={this.state.endDateMode}
                            defaultValue={this.state.endTime}
                            locale={locale}
                            showTime
                            onChange={this.onEndDateChange}
                            onPanelChange={this.handleEndPanelChange}
                        />
                        <span style={{color: '#F00'}}> *</span>
                    </div>
                    <div className="dd-row">
                        <span>数据类型: </span>
                        <Radio.Group onChange={this.onChange} value={this.state.type}>
                            <Radio value="web">web</Radio>
                            <Radio value="facebook">facebook</Radio>
                            <Radio value="twitter">twitter</Radio>
                        </Radio.Group>
                    </div>
                </div>
            </Modal>
        )
    }
}

export default DataDeleteModal;