import React from 'react';
let dragSrc = require('../images/drag.png');

class DragHandler extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isDown: false,
            lastClientY: 0
        };
    }
    onDragMouseDown = (e) => {
        this.state.isDown = true;
        e.persist();
        this.state.lastClientY = e.clientY;
    }
    onDragMouseUp = (e) => {
        this.state.isDown = false;
    }
    onDragMouseMove = (e) => {
        if (!this.state.isDown){ return null; }
        e.persist();
        e.preventDefault();
        let clientYdiff = this.state.lastClientY - e.clientY;
        this.props.onDrag(clientYdiff);
        this.state.lastClientY = e.clientY;
    }
    render(){
        return (
            <div
                className="dragbox"
                onMouseDown={this.onDragMouseDown}
                onMouseMove={this.onDragMouseMove}
                onMouseUp={this.onDragMouseUp}
                onMouseLeave={this.onDragMouseUp}
            >
                <div className="drag">
                    <img style={{width: 24, height: 20}} src={dragSrc} />
                </div>
            </div>
        )
    }
 }

 export default DragHandler;