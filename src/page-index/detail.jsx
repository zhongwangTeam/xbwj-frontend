import React from 'react';
let avatarSrc = require('../images/account.png');
import Clipboard from 'clipboard';
import { message, Spin, Icon, Select } from 'antd';
const { Option } = Select;
let pngSrc = require('../images/pdf.png');
import { Resizable } from 're-resizable';

class DetailBox extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            fontSize: '14',
            winHeight: 300
        }
    }
    componentDidMount(){
        let cb = new Clipboard('#copy');
        cb.on('success', function(e) {
            message.info('已复制');
        });
    }
    _renderImages = (image_file, root_path, image_desc) => {
        if (!image_file || image_file.length == 0){
            return null;
        }
        let imagesList;
        if (image_file.indexOf('[') >= 0){
            try {
                imagesList = JSON.parse(image_file);
            } catch (e){
                return null;
            }
        } else {
            imagesList = [image_file];
        }
        if (imagesList.length == 0){
            return null;
        }
        let imagesDesc = [];
        if (image_desc){
            if (image_desc.indexOf('[') >= 0){
                try {
                    imagesDesc = JSON.parse(image_desc);
                } catch (e){
                    return [];
                }
            } else {
                imagesDesc = [image_desc];
            }
        }
        let tmp = imagesList.map((item, index) => {
            let src = `/static/image?rootPath=${root_path}&imagePath=${item}`;
            return (

                <div key={index} className="image" onClick={() => this.props.onImageClick(src)}>
                    <img src={src} />
                    {imagesDesc[index] ? <div className="image-desc">{imagesDesc[index]}</div> : null}
                </div>
            )
        });
        return (
            <div id="images" className="images">
                {tmp}
            </div>
        );
    }
    _renderVideos = (video_file, root_path) => {
        if (!video_file || video_file.length == 0){
            return null;
        }
        let videosList;
        if (video_file.indexOf('[') >= 0){
            try {
                videosList = JSON.parse(video_file);
            } catch (e){
                return null;
            }
        } else {
            videosList = [video_file];
        }
        if (videosList.length == 0){
            return null;
        }
        let tmp = videosList.map((item, index) => {
            let src = `/static/video?rootPath=${root_path}&videoPath=${item}`;
            return (<div key={index} className="video"><video width="max-height: 200px;" src={src} controls="controls">您的浏览器不支持 video 标签。</video></div>)
        });
        return (
            <div id="videos" className="videos">
                {tmp}
            </div>
        );
    }
    _renderPdfs = (pdf_file, root_path) => {
        if (!pdf_file || pdf_file.length == 0){
            return null;
        }
        let pdfsList;
        if (pdf_file.indexOf('[') >= 0){
            try {
                pdfsList = JSON.parse(pdf_file);
            } catch (e){
                return null;
            }
        } else {
            pdfsList = [pdf_file];
        }
        if (pdfsList.length == 0){
            return null;
        }
        let tmp = pdfsList.map((item, index) => {
            let src = `/static/pdf?rootPath=${root_path}&pdfPath=${item}`;
            return (
                <div key={index} className="pdf">
                    <a target="_blank" className="pdf-box" href={src}>
                        <div className="pdf-png"><img src={pngSrc} /></div>
                        <div className="pdf-index">{index+1}</div>
                    </a>
                </div>
            );
        });
        return (
            <div id="pdfs" className="pdfs clearfix">
                {tmp}
            </div>
        );
    }
    _renderFriends = (friends) => {
        if (!friends || friends.length == 0){
            return null;
        }
        let tmp = friends.map((item, index) => {
            return (
                <div key={index} className="friend-name">{item.following_name || item.follower_name || item.friend_name || ''}</div>
            )
        })
        return (
            <div className="friend-list">
                {tmp}
            </div>
        );
    }
    _renderComments = (comments) => {
        if (!comments || comments.length == 0){
            return null;
        }
        let tmp = comments.map((item, index) => {
            return (
                <div key={index} className="fb-comment">
                    <div>{item.comment_content}</div>
                    <div className="fb-commenter clearfix">
                        <span>{item.comment_name} </span>
                        <span> {item.comment_date}</span>
                        <div style={{float: 'right'}}>
                            <span>点赞数: {item.comment_love || '--'}</span>
                        </div>
                    </div>
                </div>
            )
        })
        return (
            <div className="fb-comments">
                {tmp}
            </div>
        );
    }
    _renderContent = (type, content, regex) => {
        let tmp = content.replace(/\\t/g, '');
        if (!regex){
            if (type == 'facebook'){
                return (
                    <div className="fb-con-text">
                        {tmp.split('\\n').map((item, key) => {
                          return <span key={key}>{item}<br/></span>
                        })}
                    </div>
                );
            } else if (type == 'twitter'){
                return (
                    <div className="fb-title">
                        {tmp.split('\\n').map((item, key) => {
                          return <span key={key}>{item}<br/></span>
                        })}
                    </div>
                );
            } else {
                return (
                    <div className="content">
                        {tmp.split('\\n').map((item, key) => {
                          return <span key={key}>{item}<br/></span>
                        })}
                    </div>
                )
                // return (<div dangerouslySetInnerHTML={{__html: content}} className="content"></div>)
            }
        }
        let res = tmp.replace(regex, '<span class="highlight">$1</span>').replace(/\\n/g, '<br>');
        if (type == 'facebook'){
            return (<div className="fb-con-text" dangerouslySetInnerHTML={{__html: res}}></div>);
        } else if (type == 'twitter'){
            return (<div className="fb-title" dangerouslySetInnerHTML={{__html: res}}></div>);
        } else {
            return (<div className="content" dangerouslySetInnerHTML={{__html: res}}></div>)
        }
    }
    _renderWebContent = (data, highlightRegex) => {
        let transStyle = {height: this.state.winHeight, overflowY: 'auto'}
        if (data.closeTranslate || !data.content_cn){
            return (
                <div className="original">
                    <div className="title">{data.title}</div>
                    <div className="sumbox clearfix">
                        <div className="time">{data.time}</div>
                        <div className="author">{data.author}</div>
                    </div>
                    <div className="contextBox">
                        {this._renderContent('web', data.content || '', highlightRegex)}
                        <span id="copy" className="copy" data-clipboard-text={`${data.title} ${data.author} ${data.date} ${data.content}`}>复制</span>
                        {data.url ? <a href={data.url} target="_blank" className="view-origin">查看原文</a> : null}
                        {this._renderTranslateBtn(data)}
                    </div>
                </div>
            );
        }
        return (
            <div className="original clearfix">
                <div style={transStyle} className="part_en">
                    <div className="title">{data.title}</div>
                    <div className="sumbox clearfix">
                        <div className="time">{data.time}</div>
                        <div className="author">{data.author}</div>
                    </div>
                    <div className="contextBox">
                        {this._renderContent('web', data.content || '', highlightRegex)}
                        <span id="copy" className="copy" data-clipboard-text={`${data.title} ${data.author} ${data.date} ${data.content}`}>复制</span>
                        {data.url ? <a href={data.url} target="_blank" className="view-origin">查看原文</a> : null}
                    </div>
                </div>
                <div style={transStyle} className="part_cn">
                    <Icon onClick={() => this.props.onCloseTranslate(data)} title="关闭翻译" className="close_cn" type="close" />
                    <div className="title">{data.title_cn || data.title}</div>
                    <div className="sumbox clearfix">
                        <div className="time">{data.time}</div>
                        <div className="author">{data.author}</div>
                    </div>
                    <div className="contextBox">
                        {this._renderContent('web', data.content_cn || '', highlightRegex)}
                        <span id="copy" className="copy" data-clipboard-text={`${data.title_cn} ${data.author} ${data.date} ${data.content_cn}`}>复制</span>
                    </div>
                </div>
            </div>
        );
    }
    _renderFacebookContent = (data, highlightRegex) => {
        let transStyle = {height: this.state.winHeight, overflowY: 'auto'}
        if (data.closeTranslate || !data.post_content_cn){
            return (
                <div>
                    <div className="fb-title">{data.post_title}</div>
                    {this._renderContent('facebook', data.post_content || '[图片或视频]', highlightRegex)}
                    {data.post_url ? <a className="view-origin" target="_blank" href={data.post_url}>查看原文</a> : null}
                    {this._renderTranslateBtn(data)}
                </div>
            )
        }
        return (
            <div className="original clearfix">
                <div style={transStyle} className="part_en">
                    <div className="fb-title">{data.post_title_cn || data.post_title}</div>
                    {this._renderContent('facebook', data.post_content || '[图片或视频]', highlightRegex)}
                    {data.post_url ? <a className="view-origin" target="_blank" href={data.post_url}>查看原文</a> : null}
                </div>
                <div style={transStyle} className="part_cn">
                    <Icon onClick={() => this.props.onCloseTranslate(data)} title="关闭翻译" className="close_cn" type="close" />
                    <div className="fb-title">{data.post_title_cn || data.post_title}</div>
                    {this._renderContent('facebook', data.post_content_cn, highlightRegex)}
                </div>
            </div>
        )
    }
    _renderTwitterContent = (data, highlightRegex) => {
        let transStyle = {height: this.state.winHeight, overflowY: 'auto'}
        if (data.closeTranslate || !data.tweet_content_cn){
            return (
                <div className="original clearfix">
                    {this._renderContent('twitter', data.tweet_content || '[图片或视频]', highlightRegex)}
                    {data.tweet_url ? <a className="view-origin" target="_blank" href={data.tweet_url}>查看原文</a> : null}
                    {this._renderTranslateBtn(data)}
                </div>
            )
        }
        return (
            <div className="original clearfix">
                <div style={transStyle} className="part_en">
                    {this._renderContent('twitter', data.tweet_content || '[图片或视频]', highlightRegex)}
                    {data.tweet_url ? <a className="view-origin" target="_blank" href={data.tweet_url}>查看原文</a> : null}
                </div>
                <div style={transStyle} className="part_cn">
                    <Icon onClick={() => this.props.onCloseTranslate(data)} title="关闭翻译" className="close_cn" type="close" />
                    {this._renderContent('twitter', data.tweet_content_cn, highlightRegex)}
                </div>
            </div>
        )
        
    }
    _renderTranslateBtn = (data) => {
        let type = this.props.type;
        if ((type == 'web' && data.content_cn) || (type == 'facebook' && data.post_content_cn) || (type == 'twitter' && data.tweet_content_cn)){
            return null;
        }
        return (
            <span className="translate" onClick={() => this.props.onTranslate(type, data)}>翻译</span>
        );
    }
    _renderFontSizeSelect = () => {
        let style = this.props.type == 'web' ? { left: 10} : { left: '34%'};
        return (
            <div style={style} className="font-select">
                <span>字体大小:&nbsp;</span>
                <Select value={this.state.fontSize} style={{ width: 72 }} onChange={this.onFontSizeChange}>
                    <Option value="13">小</Option>
                    <Option value="14">中</Option>
                    <Option value="16">大</Option>
                    <Option value="20">较大</Option>
                    <Option value="24">特大</Option>
                </Select>
            </div>
        );
    }
    onFontSizeChange = (value) => {
        this.setState({fontSize: value});
    }
    onResizeStop = (e, direction, ref, d) => {
        let winHeight = this.state.winHeight + d.height;
        if (winHeight > 600){
            winHeight = 600;
        } else if (winHeight < 200){
            winHeight = 200;
        }
        this.setState({winHeight});
        this.props.onDetailWinChange({height: winHeight});
    }
    render(){
        if (!this.props.visible){ return null; }
        let data = this.props.data;
        // data.content_cn = data.content;
        // data.post_content_cn = data.post_content;
        // data.tweet_content_cn = data.tweet_content;
        let author = this.props.author;
        let comments = this.props.comments;
        let friends = this.props.friends;
        let followers = this.props.followers;
        let following = this.props.following;
        let highlightRegex;
        if (this.props.highlightWords.length > 0){
            highlightRegex = new RegExp(`(${this.props.highlightWords.join('|')})`, 'gi');
        }
        let fontStyle = {fontSize: `${this.state.fontSize}px`};
        if (this.props.type == 'facebook'){
            return (
                <div style={{height: this.state.winHeight}} className={`summary clearfix${data.tibetan ? ' tibetan' : ''}`}>
                    <Resizable
                        minHeight={200}
                        maxHeight={600}
                        size={{ height: this.state.winHeight }}
                        enable={{ top: true, right: false, bottom: false, left: false, topRight: false, bottomRight: false, bottomLeft: false, topLeft: false }}
                        onResizeStop={this.onResizeStop} >
                        <Spin spinning={this.props.detailLoading}>
                        <div style={{height: this.state.winHeight - 20}} className="summary-box">
                            <div onClick={this.props.onClose} className="close">关闭</div>
                            {this._renderFontSizeSelect()}
                            <div className="summary-inner fb-inner">
                            <div style={{height: this.state.winHeight-20}} className="scrollbox clearfix">
                                <div className="fb-fl">
                                    <div className="fb-avatar"><img width={100} src={avatarSrc} /></div>
                                    <div className="fb-name"><a href="#">{data.account_id}</a></div>
                                    <div className="fb-info">
                                        <span title="好友数">好友数: {author.account_friend || '--'}</span>
                                        <span title="所在地">所在地: {author.account_live || '--'}</span>
                                        <span title="来自">来自: {author.account_from || '--'}</span>
                                        <span title="加入 facebook 时间">加入facebook时间: {author.account_joined || '--'}</span>
                                        <span title="点赞数">点赞数: {author.account_like || '--'}</span>
                                    </div>
                                    <div className="bio">{author.basic_info || '暂无个人基本信息'}</div>
                                    <div className="friends">
                                        <div className="friend-title">好友列表</div>
                                        {this._renderFriends(friends)}
                                    </div>
                                </div>
                                <div style={fontStyle} className="fb-fr">
                                    {this._renderFacebookContent(data, highlightRegex)}
                                    <div className="fb-summary clearfix">
                                        <span title="发布日期">发布日期: <span className="date">{data.post_date || '--'}</span></span>
                                        <span title="发布地点">发布地点: <span className="locate">{data.post_place || '--'}</span></span>
                                        <span title="评论数">评论数: <span className="comments">{data.post_comment_sum || '--'}</span></span>
                                        <span title="转发数">转发数: <span className="share">{data.post_shared || '--'}</span></span>
                                        <div style={{float: 'right'}}>
                                            <span title="大爱">大爱: <span className="heart">{data.post_haha || 0}</span></span>
                                            <span title="笑趴">笑趴: <span className="laugh">{data.post_love || 0}</span></span>
                                            <span title="哇">哇: <span className="surprise">{data.post_wow || 0}</span></span>
                                            <span title="心碎">心碎: <span className="sad">{data.post_sad || 0}</span></span>
                                            <span title="愤怒">愤怒: <span className="angry">{data.post_angry || 0}</span></span>
                                        </div>
                                    </div>
                                    <div className="fb-con">
                                        <div className="fb-con-box">
                                            <div className="fb-con-text"></div>
                                            {this._renderImages(data.image_file, data.root_path, data.image_desc)}
                                            {this._renderVideos(data.video_file, data.root_path)}
                                        </div>
                                        {this._renderComments(comments)}
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </Spin>
                    </Resizable>
                </div>
            );
        } else if (this.props.type == 'twitter'){
            return (
                <div style={{height: this.state.winHeight}} className={`summary clearfix${data.tibetan ? ' tibetan' : ''}`}>
                    <Resizable
                        minHeight={200}
                        maxHeight={600}
                        size={{ height: this.state.winHeight }}
                        enable={{ top: true, right: false, bottom: false, left: false, topRight: false, bottomRight: false, bottomLeft: false, topLeft: false }}
                        onResizeStop={this.onResizeStop} >
                        <Spin spinning={this.props.detailLoading}>
                        <div style={{height: this.state.winHeight - 20}} className="summary-box">
                            <div onClick={this.props.onClose} className="close">关闭</div>
                            {this._renderFontSizeSelect()}
                            <div className="summary-inner fb-inner">
                            <div style={{height: this.state.winHeight-20}} className="scrollbox clearfix">
                                <div className="fb-fl">
                                    <div className="fb-avatar"><img width={100} src={avatarSrc} /></div>
                                    <div className="fb-name"><a href="#">{author.tweet_user_id || '--'}</a></div>
                                    <div className="fb-info">
                                        <span name="followers">被关注数: <span className="followers">{author.followers || '--'}</span></span>
                                        <span name="following">关注数: <span className="following">{author.following || '--'}</span></span>
                                        <span name="tweets">推文数: <span className="tweets">{author.tweets || '--'}</span></span>
                                        <span title="喜欢数">喜欢数: <span className="likes">{author.favorites || '--'}</span></span>
                                        <span title="加入 Twitter 时间">加入Twitter时间: <span className="date">{author.join_date || '--'}</span></span>
                                        <span title="所在地">所在地: <span className="locate">{author.location || '--'}</span></span>
                                    </div>
                                    <div className="bio">{author.bio || '暂无个人简介'}</div>
                                    <div className="friends">
                                        <div className="friend-title">关注者</div>
                                        <div className="friend-list followers">
                                            {this._renderFriends(followers)}
                                        </div>
                                        <div className="friend-title">被关注者</div>
                                        <div className="friend-list following">
                                            {this._renderFriends(following)}
                                        </div>
                                    </div>
                                </div>
                                <div style={fontStyle} className="fb-fr">
                                    {this._renderTwitterContent(data, highlightRegex)}
                                    <div className="fb-summary clearfix">
                                        <span name="time" title="发布日期">发布日期: <span className="date">{data.tweet_time || '--'}</span></span>
                                        <span name="locate" title="发布地点">发布地点: <span className="locate">{data.location || '--'}</span></span>
                                        <span name="comments" title="评论数">评论数: <span className="comments">{data.twitter_reply || '--'}</span></span>
                                        <span name="share" title="转发数">转发数: <span className="share">{data.twitter_trunsmit || '--'}</span></span>
                                        <div style={{float: 'right'}}>
                                            <span name="heart" title="大爱">大爱: <span className="heart">{data.twitter_like || '--'}</span></span>
                                            <span name="laugh" title="笑趴">笑趴: <span className="laugh">--</span></span>
                                            <span name="surprise" title="哇">哇: <span className="surprise">--</span></span>
                                            <span name="sad" title="心碎">心碎: <span className="sad">--</span></span>
                                            <span name="angry" title="愤怒">愤怒: <span className="angry">--</span></span>
                                        </div>
                                    </div>
                                    <div className="fb-con">
                                        <div className="fb-con-box">
                                            {/*<div className="fb-con-text">{data.tweet_content || '[图片或视频]'}</div>*/}
                                            <div className="fb-images"></div>
                                            <div className="fb-videos"></div>
                                            {this._renderImages(data.image_file, data.root_path, data.image_desc)}
                                            {this._renderVideos(data.video_file, data.root_path)}
                                        </div>
                                        {this._renderComments(comments)}
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </Spin>
                    </Resizable>
                </div>
            );
        }
        return (
           <div style={{height: this.state.winHeight}} id="summary" className={`summary${data.tibetan ? ' tibetan' : ''}`}>
                <Resizable
                    minHeight={200}
                    maxHeight={600}
                    size={{ height: this.state.winHeight }}
                    enable={{ top: true, right: false, bottom: false, left: false, topRight: false, bottomRight: false, bottomLeft: false, topLeft: false }}
                    onResizeStop={this.onResizeStop} >
                    <Spin spinning={this.props.detailLoading}>
                    <div className="summary-box">
                        <div onClick={this.props.onClose} className="close">关闭</div>
                        {this._renderFontSizeSelect()}
                        <div style={fontStyle} className="summary-inner">
                            <div style={{height: this.state.winHeight-20}} className="scrollbox">
                                {this._renderWebContent(data, highlightRegex)}
                                {this._renderImages(data.image_file, data.root_path, data.image_desc)}
                                {this._renderVideos(data.video_file, data.root_path)}
                                {this._renderPdfs(data.pdf_file, data.root_path)}
                            </div>
                        </div>
                    </div>
                    </Spin>
                </Resizable>
           </div>
        );
    }
}

export default DetailBox;