const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HotModuleReplacePlgun = require('webpack/lib/HotModuleReplacementPlugin');

module.exports = {

  // This option controls if and how source maps are generated.
  // https://webpack.js.org/configuration/devtool/
  devtool: 'eval-cheap-module-source-map',

  // https://webpack.js.org/concepts/entry-points/#multi-page-application
  entry: {
    index: ['react-hot-loader/patch','./src/page-index/main.js'],
    // about: './src/page-about/main.js'
  },

  // https://webpack.js.org/configuration/dev-server/
  devServer: {
    host: '0.0.0.0',
    port: 8088,
    hot: true,
    proxy: {
      '/web': {
        target: 'http://192.168.1.14:9090'
      },
      '/static': {
        target: 'http://192.168.1.14:9090'
      },
      '/twitter': {
        target: 'http://192.168.1.14:9090'
      },
      '/facebook': {
        target: 'http://192.168.1.14:9090'
      },
      '/config': {
        target: 'http://192.168.1.14:9090'
        // target: 'http://192.168.1.108:8080'
      },
      '/translate': {
        target: 'http://192.168.1.14:9090'
        // target: 'http://192.168.1.108:8080'
      },
    },
  },

  // https://webpack.js.org/concepts/loaders/
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ['@babel/preset-env','@babel/preset-react'],
            "plugins": [
              "@babel/plugin-proposal-class-properties",
              "react-hot-loader/babel"
            ]
          }
        }
      },
      {
        test: /\.css$/,
        use: [
          "style-loader",
          "css-loader"
          // Please note we are not running postcss here
        ]
      },
      {
        // Load all images as base64 encoding if they are smaller than 8192 bytes
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              // On development we want to see where the file is coming from, hence we preserve the [path]
              name: '[path][name].[ext]?hash=[hash:20]',
              limit: 8192
            }
          }
        ]
      }
    ],
  },

  // https://webpack.js.org/concepts/plugins/
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/page-index/tmpl.html',
      inject: true,
      chunks: ['index'],
      title: '首页',
      filename: 'index.html'
    }),
    new HotModuleReplacePlgun(),
    // new HtmlWebpackPlugin({
    //   template: './src/page-about/tmpl.html',
    //   inject: true,
    //   chunks: ['about'],
    //   filename: 'about.html'
    // })
  ]
};
